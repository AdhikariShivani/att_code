Testing Criteria 

1: Head intent test : Making sure that the entered test utterance is mapping to correct intent/supplement


2. Running every possible flow in unique sessions - Every test utterance and the paths , flows are run in different sessions to make sure the quality of every Use Case .  


3. Running more than one test utterance end to end in the same session to make sure the end of each session is flagged to start flow. (which is different test utterances coming from different use cases) 


4:NLU testing:Testing with number of synthetic utterances and making sure bot is accepting all possible utterances for each use case


5:Testing with different parameter configurations(BAN numbers) for each of the head intent and making sure the responses have the right parameter set for them


6:Testing  with and without parameter set for  test utterances 


7:Verifying buttons within each of the responses ina flow that  leads to right intents and correct custom payloads


9:Response/Payload Testing - verifying custom payload is showing correctly for each uses cases and is not empty/duplicated and has the right content


10:Testing with webhooks on /off


11:Negative scenarios(Testing with incorrect parameter configurations/utterances) to make sure bot doesn't break in case user enters something that's not expected

12:  Testing with multiple chip buttons ( with and without back nav)

