import logging
import csv
import os
import uuid
import os.path
import json
from os import path
from xlsxwriter.workbook import Workbook
from google.cloud.dialogflowcx_v3beta1.services.sessions import SessionsClient
from google.cloud.dialogflowcx_v3beta1.types import session, agent, intent, page, flow

class DialogflowAgentTester:
    LOG_FILENAME = 'example.log'
    logging.basicConfig(filename=LOG_FILENAME,level=logging.INFO)

    def __init__(self, intent_to_run, result_file, creds, agent_id):
        self.intent_to_run = intent_to_run
        self.result_file = result_file
        self.agent_id = agent_id
        self.creds = creds

    def detect_intent_texts(self, agent_id, session_id, text, language_code, parameters=None):
        """Returns the result of detect intent with texts as inputs.

      Using the same `session_id` between requests allows continuation
      of the conversation."""
        os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = self.creds
        session_client = SessionsClient()
        session_path = "{}/sessions/{}".format(agent_id, session_id)
        text_input = session.TextInput(text=text)
        query_input = session.QueryInput(text=text_input, language_code=language_code)
        if type(parameters) is dict:
            query_params = session.QueryParameters(parameters=parameters)
            request = session.DetectIntentRequest(
                session=session_path, query_input=query_input, query_params=query_params
            )
        else:
            request = session.DetectIntentRequest(
                session=session_path, query_input=query_input
            )
        response = session_client.detect_intent(request=request)
        return response, response.query_result.response_messages, response.query_result.parameters, response.query_result.match.intent

    def convert_to_xls(csvfile):
        workbook = Workbook(csvfile[:-4] + '.xlsx')
        worksheet = workbook.add_worksheet('results')
        with open(csvfile, 'rt', encoding='utf8') as f:
            reader = csv.reader(f)
            for r, row in enumerate(reader):
                for c, col in enumerate(row):
                    worksheet.write(r, c, col)
        workbook.close()

    def write_result(self, utterance_name, Result, actual_response, expected_response, param):
        """
        Writes the result to a csv file
        """
        if path.exists(self.result_file):
            with open(self.result_file, 'a') as outFile:
                # else open file and add to last
                fileWriter = csv.writer(outFile)
                if 'Fail::The intent name doesnt match' in Result:
                    fileWriter.writerow([utterance_name, Result, actual_response, expected_response, ])
        else:
            with open(self.result_file, 'w') as outFile:
                fileWriter = csv.writer(outFile)
                fileWriter.writerow(['Utterance', 'Result', 'Actual Response', 'Expected Response', 'param'])
                if 'Fail::The intent name doesnt match' in Result:
                    fileWriter.writerow([utterance_name, Result, actual_response, expected_response, param])

    def validate_conditions(self, actual_result, additional_params, expected_result, intent_name):
        """
    Validates the conditions that can result failure and returns if there is any failures
     """
        Result = 'Pass'
        if type(expected_result) is str:
            expected_result = json.loads(expected_result)
        # Check if the bot response returns the expected intent

        if intent_name.display_name not in expected_result['intent_name']:
            return "Fail::The intent name doesnt match, Getting : " + intent_name.display_name
        flag = False

        if actual_result != [] and actual_result[0].payload is not None:
                if actual_result[0].payload['message'] in expected_result['message']:
                    Result = "Pass"
                else:
                    Result = "Fail:The actual result and expected result do not match"
        else:
            Result = "Fail::The actual result is null"
        return Result

    def validate_parameter_set(self, response):
        Flag = True
        for param_fields in response.query_result.parameters:
            logging.info(param_fields)
            logging.info("---------------")
            logging.info(response.query_result.parameters[param_fields])
            if response.query_result.parameters[param_fields]:
                param_empty = []
                param_empty.append(param_fields)
                Flag = False
        if Flag:
            Result = 'Pass::The bot was able to set the parameters'
        else:
            Result = 'Fail:Unable to set value for parameter :::: ' + str(param_empty)
        return Result

    def test_aam_intents_conversations(self, utterance_list, new_session, data):
        """
     Returns the test result when an utterance is passed for each of the head intents
     """

        Result = actual_result = params = ''
        data = json.loads(data)
        session_id = uuid.uuid4()

        if len(utterance_list) == 1 and utterance_list[0] == '**':
            logging.info('Setting the parameter')
            response, actual_result, params, intent_name = self.detect_intent_texts(self.agent_id, session_id,
                                                                                    data['utterance'], 'en')
            self.validate_parameter_set(response)

        elif 'scenarios' in data.keys():
            for sc in data['scenarios']:

                if 'utterances_from_file' in sc.keys():
                    # check if utterance from csv file
                    self.run_validate_utterance_from_file(sc, utterance_list, new_session)
                    continue

                if new_session.lower() == 'true':
                    session_id = uuid.uuid4()
                    logging.info('Running main utterance :' + sc['utterance'])
                    utterance_list = [*utterance_list, sc['utterance']]
                    for uttr in utterance_list:

                        if sc['expected_message']['parameters'] is not None:
                            response, actual_result, params, intent_name = self.detect_intent_texts(self.agent_id,
                                                                                                    session_id, uttr,
                                                                                                    'en',
                                                                                                    sc
                                                                                                    ['expected_message']
                                                                                                    ['parameters'])
                        # validate the conditions
                        else:
                            response, actual_result, params, intent_name = self.detect_intent_texts(self.agent_id,
                                                                                                    session_id, uttr,
                                                                                                    'en')

                        #
                        Result = self.validate_conditions(actual_result, params, sc['expected_message'], intent_name)
                else:
                    logging.info('WARNING :: There are no scenarios to run for this utterance.')

        return Result, actual_result, params

    def run_validate_utterance_from_file(self, sc, utterance_list, new_session):
        result = actual_result = params = ''
        logging.info("__________________")
        logging.info("Reading utterances from file :" + sc['utterances_from_file'])
        session_id = uuid.uuid4()
        if not sc['run_in_new_session']:
            for uttr in utterance_list:
                response, actual_result, params, intent_name = self.detect_intent_texts(self.agent_id, session_id, uttr,
                                                                                        'en')
        # keep utterance list length
        utterance_list_len = len(utterance_list)
        with open(sc['utterances_from_file']) as csv_file:
            csv_reader = csv.reader(csv_file)
            line_count = 0
            for row in csv_reader:
                if line_count == 0:
                    line_count += 1
                else:
                    logging.info("__________________")
                    logging.info("Running scenario utterance : " + row[0])
                    if line_count == 1 or len(row[1]) != 0:
                        expected_result = row[1]

                    # Add row[0] utterance to utterance_list and remove the previous utterance
                    if len(utterance_list) > utterance_list_len:
                        utterance_list = utterance_list[:-1]
                    utterance_list = [*utterance_list, row[0]]

                    if sc['run_in_new_session']:
                        session_id = uuid.uuid4()
                        for uttr in utterance_list:

                            response, actual_result, params, intent_name = self.detect_intent_texts(self.agent_id,
                                                                                                    session_id, uttr,
                                                                                                    'en')
                    else:
                        response, actual_result, params, intent_name = self.detect_intent_texts(self.agent_id,
                                                                                                session_id,
                                                                                                row[0], 'en')

                    # validate the conditions
                    result = self.validate_conditions(actual_result, params, expected_result, intent_name)
                    logging.info("RESULT : " + result)
                    self.write_result('> '.join(utterance_list), result, actual_result, expected_result, params)
                    line_count += 1


# This sub module is for test_multiple_utterance_seperate_session
def nested_chip_button_backnav(utterences, session_id, expected_result,intent_to_run):
    parent_utterence = utterences
    
    actual_result,additional_params = detect_intent_texts(agent_id,session_id,parent_utterence,'en')

    #First Validation - If the intent is present
    if additional_params.intent.display_name in intent_to_run:
        #Second Validation - If actual result from bot or payload is not empty
        if actual_result != "" and actual_result[0].payload is not None:
            #Third Validation - When values are present
            if 'values' in actual_result[0].payload and actual_result[0]:
                #Looping through the values in the payload parent value node
                for item in actual_result[0].payload['values']:
                    parent_counter = 0
                    for value in item['0']:
                        parent_value = value['{}'.format(parent_counter)]
                        actual_result_child = detect_intent_texts(agent_id,session_id,parent_value,'en')
                        if actual_result_child[0] != "" :
                            if 'values' in actual_result_child[0][0].payload:
                                print (actual_result_child[0][0].payload)
                                #Looping through the values in the payload for child value node
                                for item in actual_result_child[0][0].payload['values']:
                                        child_counter = 0
                                        for value2 in item['0']:
                                            child_value = value2['{}'.format(child_counter)]
                                            actual_result_child_node = detect_intent_texts(agent_id,session_id,child_value,'en')
                                            print("Here-------------" + str(actual_result_child_node))
                                            if actual_result_child_node[0][0].text is None:
#                                                 print(actual_result_child_node[0][0].payload['message'])
                                                if actual_result_child_node[0][0].payload['message'] in expected_result:
                                                    Result = "Child Node Validation Passed"
#                                                     print (Result)
                                                elif actual_result_child_node[0][0].text.text[0]:
                                                    Result = "There is a text"
                                                else:
                                                    Result = "Child Node Validation Failed"
                                            else:
                                                Result = ("There is a text: " + str(actual_result_child_node[0][0].text))
                                            child_counter = child_counter + 1
                            elif 'text' in actual_result_child[0][0].payload:
                                Result = ("There is a text")
                                print(Result)
                            elif 'message' in actual_result_child[0][0].payload:
#                                 print (actual_result_child[0][0].payload['message'] )
                                if actual_result_child[0][0].payload['message'] in expected_result:
                                    Result = "Passed"
                                else:
                                    Result = "Failed - Do not match the expected result"
                            else:
                                Result = "The Payload is Empty - Validation Failed"
#                                 if actual_result_child[0][0].payload['message'] in expected_result:
#                                     Result = "Passed"
#                                 else:
#                                     Result = "Failed - Do not match the expected result"
                        else:
                            Result = "The Payload is Empty - Validation Failed"
                        parent_counter = parent_counter + 1
                        session_id = uuid.uuid4()
                        actual_result,additional_params = detect_intent_texts(agent_id,session_id,parent_utterence,'en')
            elif actual_result[0].payload['message'] in expected_result:
                Result = "Passed"
            else:
                Result = "The payload is empty - Validation Failed"
        else:
            #Validation when text is present but there is not payload
            if 'text' in actual_result[0]:
                if actual_result[0].text.text[0] in expected_result:
                    Result = "Passed"
                else:
                    Result = "Failed - There is no text / payload"
            else:
                Result = "Failed - There is no text / payload"
    else:
        #If the intent is not present the validation fails
        Result = "Fail::The intent name doesnt match, Getting : "+ additional_params.intent.display_name
    print (Result)
    return Result, actual_result, additional_params

def read_multiple_utterance_seperate_session(flag, intent_to_run):   
    print("-----Execution Started for "+intent_to_run+"-----")
    # define a list of utterances to test  
    
    #call the function to delete the file if exist
    del_file_from_results(intent_to_run)

    # open a .CSV file with the given intent_to_run name  
    with open('intents/'+intent_to_run+'.csv') as csv_file:
#         flag = "multiple_utterance"
        csv_reader = csv.reader(csv_file)
        next(csv_reader, None)
        line_count = 1
        Result = ''
        tempSessionID = 0
        tempSessionNumber = 0

        for row in csv_reader:
            if line_count == 0:
                line_count += 1
            else:
                print("Running utterance: "+ row[0])
                if line_count == 1 or len(row[1]) != 0:
                    expected_result= row[1]
                if flag == "multiple_utterance":
                    if (row[2] != tempSessionNumber):
                        tempSessionNumber = row[2]
    #                   This session ID is used to send the sets if Utterences to the chat bot from the single CSV, There is no single identifier in the CSV where we know how to group the utterences. Until and unless we want to send seperate files 
                        session_id = uuid.uuid4()
                    else:
                        tempSessionID = session_id
                    Result=test_multiple_responses(row[0], session_id, expected_result)
                    actual_result,additional_params = detect_intent_texts(agent_id,session_id,row[0],'en')
            line_count += 1
            write_result(row[0],Result,actual_result,row[1],additional_params,intent_to_run)
        print("-----Execution Ended-----"+intent_to_run+"-----")
        
def test_multiple_responses(utterances, session_id, expected_result):
    """
    Returns the test result when an utterance without a param is passed
    """
    # get the bot response for the first utterance
    actual_result,additional_params = detect_intent_texts(agent_id,session_id,utterances,'en')
    if additional_params.intent.display_name in intent_to_run: 
        flagResult = "Fail"
    
        for item in actual_result[0].payload['values']:
            i = 0
            for value in item['0']:
                if str(value['{}'.format(i)]) in expected_result:
                    flagResult = "Pass"
                i = i + 1
            if flagResult == "Pass":
                Result = "Pass"
            else:
                Result = "Fail"
    else:
        #If the intent is not present the validation fails
        Result = "Fail::The intent name doesnt match, Getting : "+ additional_params.intent.display_name
    return Result


#####New###########
# This sub module is for test_multiple_utterance_seperate_session
def read_nested_chip_buttons(utterences, session_id, expected_result,intent_to_run):
    parent_utterence = utterences
    
   
    actual_result,additional_params = detect_intent_texts(agent_id,session_id,parent_utterence,'en')
    
        
#     print("Printing actual result-------------" + str(actual_result))
#     print("Printing additional paramters----------"+ str(additional_params))
    
    flag = 0

    #First Validation - If the intent is present
    if additional_params.intent.display_name in intent_to_run:
        #Second Validation - If actual result from bot or payload is not empty
        if actual_result != "" and actual_result[0].payload is not None:
            #Third Validation - When values are present
            if 'values' in actual_result[0].payload and actual_result[0]:
                #Looping through the values in the payload parent value node
                for item in actual_result[0].payload['values']:
                    parent_counter = 0
                    for value in item['0']:
                        parent_value = value['{}'.format(parent_counter)]
                        print("parent entered utterance is ///// " + parent_utterence )
                        print("first parent value is ///////" + parent_value )
                        actual_result_child = detect_intent_texts(agent_id,session_id,parent_value,'en')
                        if actual_result_child[0] != "" :
                            print("parent entered utterance is ++++++++ " + parent_utterence )
                            print("first parent value is +++++++" + parent_value )
                            if 'values' in actual_result_child[0][0].payload:
                                #Looping through the values in the payload for child value node
                                for item in actual_result_child[0][0].payload['values']:
                                        child_counter = 0
                                        for value2 in item['0']:
                                            child_value = value2['{}'.format(child_counter)]
                                            child_counter = child_counter + 1
                                            session_id = uuid.uuid4()
                                            child_level_validation = detect_intent_texts(agent_id,session_id,parent_utterence,'en')
                                            child_level_validation = detect_intent_texts(agent_id,session_id,parent_value,'en')
                                            child_level_validation = detect_intent_texts(agent_id,session_id,child_value,'en')
                                            print("parent entered utterance is //////" + parent_utterence )
                                            print("first parent value is /////" + parent_value )
                                            print("child value is /////" + child_value)
                                            if child_level_validation[0] != "":
                                                if not 'text' in child_level_validation:
    #                                                 print(actual_result_child_node[0][0].payload['message'])
                                                    print(child_level_validation[0][0].payload['message'])
                                                    if child_level_validation[0][0].payload['message'] in expected_result:
                                                        Result = "Node Validation Passed"
    #                                                     print (Result)
                                                    elif 'text' in child_level_validation[0]:
#                                                     elif '' child_level_validation[0][0].text.text[0]:
                                                        Result = "Node Validation Passed"
                                                    else:
                                                        Result = "Node Validation Failed"
                                                        print("it is failing here-------------------" +Result)
                                                        flag = 1
                                                else:
                                                    Result = ("There is a text: " + str(child_level_validation[0][0].text))
                                            else:
                                                Result = "Node Validation Failed - Payload Empty"
                                                flag = 1
                                                print("it is failing here-------------------" +Result)
                            elif 'text' in actual_result_child[0][0].payload:
                                Result = "There is a text"
                            elif 'message' in actual_result_child[0][0].payload:
                                if actual_result_child[0][0].payload['message'] in expected_result:
                                    Result = "Validation Passed"
                                else:
                                    Result = "Validation Failed - Expected result not met"
                                    print("it is failing here-------------------" +Result)
                                    flag = 1
                            else:
                                print("parent entered utterance is ---------- " + parent_utterence )
                                print("first parent value is -------------" + parent_value )
                                Result = "Validation Failed-----------------"
                                flag = 1
                                print("it is failing here-------------------" +Result)
                        else:
                            Result = "Validation Failed - Payload Empty"
                            print("parent entered utterance is **** " + parent_utterence )
                            print("first parent value is *****" + parent_value )
                            flag = 1
                            print("it is failing here-------------------" +Result)
                        parent_counter = parent_counter + 1
                        session_id = uuid.uuid4()
                        actual_result,additional_params = detect_intent_texts(agent_id,session_id,parent_utterence,'en')
            elif actual_result[0].payload['message'] in expected_result:
                Result = "Passed"
            else:
                Result = "Validation Failed - Payload Empty"
                print("it is failing here-------------------" +Result)
                flag = 1
        elif actual_result == "" or actual_result[0].payload is None:
            Result = "Validation Failed - No text or Payload--------"
            print("it is failing here-------------------for " +Result)
            flag = 1
        else:
            #Validation when text is present but there is not payload
            if 'text' in actual_result[0]:
                if actual_result[0].text.text[0] in expected_result:
                    Result = "Passed"
                else:
                    Result = "Validation Failed - No text or Payload"
                    print("it is failing here-------------------" +Result)
                    flag = 1
            else:
                Result = "Validation Failed - No text or Payload"
                print("it is failing here-------------------" +Result)
                flag = 1 
            
    else:
        #If the intent is not present the validation fails
        Result = "Fail::The intent name doesnt match, Getting : "+ additional_params.intent.display_name
#         flag = 1
    if flag == 1:
        
        Result = "Failed"
    
    print (Result)
    return Result, actual_result, additional_params




       
#Ream me first
# flag - 'multiple_utterence'
# Intent - 'B3_I_WANT_TO_CANCEL_MY_SERVICE' ,'check_for_param','testfile'  are csv file as an "intent" 
# Subfunction name - function you intend to run 

# to run nested  multiple chip buttons 

read_file_utterences('multiple_utterance','bill_vague_help', 'read_nested_chip_buttons')


# to run parameter check 
# read_file_utterences('multiple_utterance','check_for_param','parameter_check')

#to run multiple utterance - same session 

# read_multiple_utterance_seperate_session('multiple_utterance','testfile')


    def get_response_from_csv(self):
        try:
            logging.info("-----Execution Started for " + self.intent_to_run + "-----")
            # define a list of utterances to test
            # delete file from results folder
            file_name_with_path = self.result_file
            if path.exists(file_name_with_path):
                os.remove(file_name_with_path)

            # create a separate session for each utterance to test
            session_id = uuid.uuid4()

            # open a .CSV file with the given intent_to_run name
            with open(self.intent_to_run) as csv_file:
                csv_reader = csv.reader(csv_file)
                line_count = 0
                for row in csv_reader:
                    if line_count == 0:
                        line_count += 1
                    else:
                        logging.info("__________________")
                        logging.info("Running scenario utterance : " + row[0])
                        if line_count == 1 or len(row[1]) != 0:
                            utterance_list = row[0].split('>')
                            data = row[1]
                            new_session = row[2]
                        if new_session.lower() == 'true':
                            session_id = uuid.uuid4()
                        # get the bot response
                        Result, actual_result, params = self.test_aam_intents_conversations(utterance_list,
                                                                                            new_session.lower(), data)
                        # Write the final results to the csv file
                        self.write_result(row[0], Result, actual_result, data, params)
                        logging.info("Result : " + Result)
                        line_count += 1
                        logging.info("-----Execution Ended-----")
            return True
        except IOError:
            logging.info("Error: can\'t find file or read data")
            return False
